﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DeadMosquito.AndroidGoodies;
public class StartGPS : MonoBehaviour {


	IEnumerator Start () {

#if UNITY_ANDROID && ! UNITY_EDITOR

        while (AGGPS.IsGPSEnabled())
        {   
            AGSettings.OpenSettingsScreen(AGSettings.ACTION_LOCATION_SOURCE_SETTINGS);
            yield return new WaitForSeconds(3);   
        }
#endif


        yield break;

    }
	
	void Update () {
        
    }
}
