﻿using DeadMosquito.AndroidGoodies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneTo : MonoBehaviour {

    public string _phoneNumber;

    public void DialNumber() { Application.OpenURL("tel://" + _phoneNumber); }
    public void PhoneToContact()
    {
        AGDialer.PlacePhoneCall(_phoneNumber);
    }
}
