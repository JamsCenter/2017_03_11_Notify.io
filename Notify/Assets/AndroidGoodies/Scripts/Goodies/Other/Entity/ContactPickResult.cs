﻿#if UNITY_ANDROID
using System.Collections.Generic;

namespace DeadMosquito.AndroidGoodies
{
    /// <summary>
    /// Contact pick result.
    /// </summary>
    public sealed class ContactPickResult
    {
        /// <summary>
        /// Gets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public string DisplayName { get; private set; }

        /// <summary>
        /// Gets the photo URI. To load image as Texture2D use <see cref="AGFileUtils.ImageUriToTexture2D"/> passing this URI
        /// </summary>
        /// <value>The photo URI.</value>
        public string PhotoUri { get; private set; }

        /// <summary>
        /// Gets the phones.
        /// </summary>
        /// <value>The phones.</value>
        public List<string> Phones { get; private set; }

        /// <summary>
        /// Gets the emails.
        /// </summary>
        /// <value>The emails.</value>
        public List<string> Emails { get; private set; }

        public ContactPickResult(string name, string photoUri, List<string> phones, List<string> emails)
        {
            DisplayName = name;
            PhotoUri = photoUri;
            Phones = phones;
            Emails = emails;
        }

        public override string ToString()
        {
            return string.Format("[ContactPickResult: DisplayName={0}]", DisplayName);
        }
    }
}
#endif
