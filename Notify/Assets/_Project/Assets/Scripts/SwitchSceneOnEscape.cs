﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSceneOnEscape : MonoBehaviour {

    public string _sceneRedirection;
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {

            if (string.IsNullOrEmpty(_sceneRedirection))
                Application.LoadLevel(0);
            else
                Application.LoadLevel(_sceneRedirection);
        }
	}
}
